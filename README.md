# Meeting Cost Clock for Android
<img alt="Logo" src="fastlane/metadata/android/en-US/images/icon.png" width="120" />


About Meeting Cost Clock for Android
====================================

(c) 2011-2021 by Markus Hoffmann

This little app can show how expensive a meeting will be, and it shows this in 
big letters, so anyone attending the meeting can see.

The Meeting-Cost-Clock is a clock which measures the time in a currency 
value (EURO or Dollar).

It can show how expensive a meeting will be taking into account the number of
participants and a mean value of a price per person per hour. 

Press MENU-->Settings to confgure.
You can adjust the number of participants with the + and - Buttons 
(even during the meeting). 

Place the Meeting-Cost-Clock well visible for everybody on the table. 

Time is Money.

A good meeting should be only attended by people which can contribute.
A good meeting will not take longer than an hour without a break.

The best meeting is the one, which need not take place. 


Sitzungs-Kosten-Uhr fuer Android
================================

(c) 2011-2021 by Markus Hoffmann 

Zeigt auf, wie teuer eine Sitzung wird.

Die Sitzungs-Kosten-Uhr ist eine Stoppuhr mit der Besonderheit, daß sie die
Zeit in EURO mißt. Sie kann aufzeigen, wie teuer eine Sitzung wird.  Hierzu
werden die Anzahl der Teilnehmer und ein durchschnittlicher Stundensatz 
berücksichtigt. 

Der Stundensatz und noch andere Einstellungen können über MENU->Settings
vorgenommen werden. Die Anzahl der Teilnehmer kann mit den +  und - Tasten
angepasst werden, auch noch während der Sitzung.

Stellen Sie die Uhr gut sichbar für alle Teilnehmer auf, und erläutern Sie
ihren Sinn; nämlich die Siztung möglichst effizient zu gestalten, denn 
###Zeit ist Geld###.

Eine gute Sitzung sollte nur Teilnehmer haben, welche auch beitragen. Eine gute 
Sitzung dauert nie länger als eine Stunde ohne Pause.

Die beste Sitzung ist die, welche nicht stattzufinden braucht.

### Download

[<img src="https://f-droid.org/badge/get-it-on.png" alt="Get it on F-Droid" height="80">](https://f-droid.org/en/packages/de.hoffmannsgimmicks/)


### Screenshots

<div style="display:flex;">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/1.png" width="30%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/2.png" width="30%">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/3.png" width="30%">
</div>
<div style="display:flex;">
<img alt="App image" src="fastlane/metadata/android/en-US/images/phoneScreenshots/4.png" width="30%">
</div>


### Important Note:

No software can be perfect. We do our best to keep this app bug free, 
improve it and fix all known errors as quick as possible. 
However, this program is distributed in the hope that it will 
be useful, but WITHOUT ANY WARRANTY; without even the implied 
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
Use this program on your own risk.


    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

Building the .apk file
======================

The build uses the gradle environment. A ./gradlew build should do.

 git clone git@codeberg.org:kollo/MeetingCostClock.git

then do a 
  cd MeetingCostClock
  ./gradlew build
(Enter passwords for the keystore)
(the compile process will take a while.)

The apk should finally be in build/outputs/apk/MeetingCostClock-release.apk

