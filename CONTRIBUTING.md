Guide to contributing to Meeting Cost Clock for Android
========================================================

The App has been in the Google Play Store from 2011 to 2014.

I have stopped development on this app myself, because I think it is quite
complete. If someone wants to further contribute I suggest following
improvements:

1. Translate the included help/documentation of all settings into more languages
   (and complete the german translation).

2. improve the look-and-feel a little further.

3. Does someone like to make a new icon/logo?

4. Maybe add new features for speakers. But KISS!

## License and attribution

All contributions must be properly licensed and attributed. If you are
contributing your own original work, then you are offering it under a CC-BY
license (Creative Commons Attribution). If it is code, you are offering it under
the GPL-v2. You are responsible for adding your own name or pseudonym in the
Acknowledgments file, as attribution for your contribution.

If you are sourcing a contribution from somewhere else, it must carry a
compatible license. The project was initially released under the GNU public
licence GPL-v2 which means that contributions must be licensed under open
licenses such as MIT, CC0, CC-BY, etc. You need to indicate the original source
and original license, by including a comment above your contribution. 

## Contributing with a Pull Request

The best way to contribute to this project is by making a pull/merge-request:

1. Login with your codeberg account or create one now.
2. [Fork](https://codeberg.org/kollo/MeetingCostClock.git) the MeetingCostClock 
   for Android repository. 
   Work on your fork.
3. Create a new branch on which to make your change, e.g.
   `git checkout -b my_code_contribution`, or make the change on the `new` 
   branch.
4. Edit the file where you want to make a change or create a new file in 
   the `contrib` directory if you're not sure where your contribution might fit.
5. Edit `ACKNOWLEGEMENTS` and add your own name to the list of contributors 
   under the section with the current year. Use your name, or a codeberg ID, 
   or a pseudonym.
6. Commit your change. Include a commit message describing the correction.
7. Submit a pull-request against the MeetingCostClock for Android repository.

## Contributing with an Issue

If you find a mistake and you're not sure how to fix it, or you don't know how
to do a pull/merge-request, then you can file an Issue. Filing an Issue will help us
see the problem and fix it.

Create a [new Issue](https://codeberg.org/kollo/MeetingCostClock/issues/new?issue) now!

## Thanks

Thanks to R. Baarfuss for inspiration!


best regards
Markus Hoffmann, October 2015

