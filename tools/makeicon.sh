# make the icons from master in correct size and density
# (c) 2013 Markus Hoffmann
#

SOURCEFILE=logo/meetinguhr.eps


# convert +antialias -density 100 $SOURCEFILE  -antialias -resize           72x72  src/main/res/drawable/meetinguhrlogo.png
# convert +antialias -density 75  $SOURCEFILE  -antialias -adaptive-resize  48x48  src/main/res/drawable-mdpi/meetinguhrlogo.png

convert +antialias -density 75 $SOURCEFILE -antialias -resize  36x36  src/main/res/drawable-ldpi/meetinguhrlogo.png
convert +antialias -density 75  $SOURCEFILE -antialias -resize  48x48  src/main/res/drawable-mdpi/meetinguhrlogo.png
convert +antialias -density 140 $SOURCEFILE -antialias -resize  72x72  src/main/res/drawable-hdpi/meetinguhrlogo.png
convert +antialias -density 180 $SOURCEFILE -antialias -resize  72x72  src/main/res/drawable/meetinguhrlogo.png
convert +antialias -density 140 $SOURCEFILE -antialias -resize  96x96  src/main/res/drawable-xhdpi/meetinguhrlogo.png
convert +antialias -density 180 $SOURCEFILE -antialias -resize 144x144 src/main/res/drawable-xxhdpi/meetinguhrlogo.png
